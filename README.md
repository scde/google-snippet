# google-snippet

Extract and append snippets from and to google searches.

## Usage

    usage: google-snippet [-h] [-i html [html ...]] [-n [N [N ...]]] [-o [output]]
                          [-k] [-r] [--overwrite-output] [-c]
    
    Extract google snippets.
    
    optional arguments:
      -h, --help            show this help message and exit
      -i html [html ...], --input html [html ...]
                            Path to HTML file(s). Looks up all <div class="g">
                            tags.
      -n [N [N ...]], --snippet_numbers [N [N ...]]
                            Number(s) of the snippet(s) you want to extract.
                            Extracts all by default.
      -o [output], --output [output]
                            Path to output HTML file. Defaults to "./out.html". If
                            output file already exists it is used as template and
                            snippets from input are appended. When there is no
                            fitting template available it just writes raw
                            snippets.
      -k, --keep-highlight  Keeps highlighting/boldness of search terms in input
                            HTML. Default behaviour is striping <em> and <b> tags.
      -r, --random          Randomize snippet order of output HTML.
      --overwrite-output    Overwrites snippets in output HTML.
      -c, --csv             Logs snippet order of output in csv file. WARNING:
                            Overwrites last csv.
